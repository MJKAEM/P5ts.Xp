namespace p5ts {
	import Renderers = Constants.Renderers;

	export class p5 {
		private renderer: p5xp.Renderer;
		private width: number;
		private height: number;

		public constructor() {
		}

		public createCanvas(width: number, height: number, renderer: Renderers = Renderers.P2D) {
			if (this.renderer !== null) {
				throw "The canvas is already created.";
			}

			this.width = Number(width.toFixed());
			this.height = Number(height.toFixed());
			
			if (renderer === Renderers.P2D) {
				//
			} else /* if (renderer === Renderers.WEBGL) */{
				//
			}
		}
	}
}