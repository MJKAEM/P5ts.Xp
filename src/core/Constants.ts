namespace p5ts {
	export namespace Constants {
		export enum Renderers {
			P2D,
			WEBGL
		}

		export enum MouseIcons {
			ARROW,
			CROSS,
			HAND,
			MOVE,
			TEXT,
			WAIT
		}

		//
		// The PI constants would be here, but they are dropped to encourage
		// use of Math.PI...
		//

		export enum AngleModes {
			DEGREES,
			RADIANS
		}

		export enum Shapes {
			CORNER,
			CORNERS,
			RADIUS,
			RIGHT,
			LEFT,
			CENTER,
			TOP,
			BOTTOM,
			BASELINE,
			POINTS,
			LINES,
			LINE_STRIP,
			LINE_LOOP,
			TRIANGLES,
			TRIANGLE_FAN,
			TRIANGLE_STRIP,
			QUADS,
			QUAD_STRIP,
			CLOSE,
			OPEN,
			CHORD,
			PIE,
			PROJECT,
			SQUARE,
			ROUND,
			BEVEL,
			MITER
		}

		export enum ColorModes {
			RGB,
			HSB,
			HSL
		}

		export enum Inputs {
			ALT = 18,
			BACKSPACE = 8,
			CONTROL = 17,
			DELETE = 46,
			DOWN_ARROW = 40,
			ENTER = 13,
			ESCAPE = 27,
			LEFT_ARROW = 37,
			OPTION = 18,
			RETURN = 13,
			RIGHT_ARROW = 39,
			SHIFT = 16,
			TAB = 9,
			UP_ARROW = 38,
		}

		export enum BlendModes {
			BLEND,
			ADD,
			DARKEST,
			LIGHTEST,
			DIFFERENCE,
			EXCLUSION,
			MULTIPLY,
			SCREEN,
			REPLACE,
			OVERLAY,
			HARD_LIGHT,
			SOFT_LIGHT,
			DODGE,
			BURN
		}

		export enum ImageFilters {
			THRESHOLD,
			GRAY,
			OPAQUE,
			INVERT,
			POSTERIZE,
			DILATE,
			ERODE,
			BLUR,
		}

		export enum TextStyles {
			NORMAL,
			ITALIC,
			BOLD
		}

		export enum VertexModes {
			LINEAR,
			QUADRATIC,
			BEZIER,
			CURVE,
		}

		export const DEFAULT_TEXT_FILL = '#000000';
		export const DEFAULT_LEADMULT = 1.25;
		export const DEFAULT_STROKE = '#000000';
		export const DEFAULT_FILL = '#FFFFFF';
	}
 }
 