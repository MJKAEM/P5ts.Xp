namespace p5ts {
	export namespace DOM {
		export class Element {
			private element: HTMLElement;
			private width: number;
			private height: number;
			private events: any;

			public constructor(element: HTMLElement, p5Instance: p5ts.p5) {
				this.element = element;
				this.width = element.offsetWidth;
				this.height = element.offsetHeight;
				this.events = {};
			}

			/**
			 * Returns the current parent node of this element.
			 * 
			 * @returns {Node} the parent node of this element
			 * 
			 * @memberOf Element
			 */
			public parent(): Node;

			/**
			 * Sets the element to be the child of the specified parent node by referencing the parent's ID, the node, or the p5.Element. The parent node must exist for this to work.
			 * 
			 * @param {((string | Element))} parent
			 * 
			 * @memberOf Element
			 */
			public parent(parent: (string | Element)): void;

			public parent(parent?: (string | Element | Node)) : (Node | void) {
				if (arguments.length === 0) {
					return this.element.parentNode;
				} else /* if (arguments.length === 1) */ {
					// Locate the parent node from the provided arguments and append this node as a child.
					let locatedParent: Node;

					// If using ID, or if using DOM Node / p5.Element.
					if (parent instanceof String) {
						let id: string = parent;
						if (id[0] === "#") {
							id = id.substring(1);
						}
						locatedParent = document.getElementById(id);
					} else if (parent instanceof Element) {
						locatedParent = parent.element;
					} else /* if (parent instanceof Node) */ {
						locatedParent = parent;
					}

					locatedParent.appendChild(this.element);
				}
			}

			/**
			 * Returns the current ID of this element.
			 * 
			 * @returns {string} the ID of this element
			 * 
			 * @memberOf Element
			 */
			public id(): string;

			/**
			 * Sets the ID of this element.
			 * 
			 * @param {string} newId the new id of this element
			 * 
			 * @memberOf Element
			 */
			public id(newId: string): void;

			public id(newId?: string): (string | void) {
				if (arguments.length === 0) {
					return this.element.id;
				} else /* if (arguments.length === 1) */ {
					this.element.id = newId;
				}
			}

			/**
			 * Returns the current class(es) of this element.
			 * 
			 * @returns {string} the class(es) of this element
			 * 
			 * @memberOf Element
			 */
			public class(): string;

			/**
			 * Sets the class of this element.
			 * 
			 * @param {string} newClass the new class of this element
			 * 
			 * @memberOf Element
			 */
			public class(newClass: string): void;

			public class(newClass?: string): (string | void) {
				if (arguments.length === 0) {
					return this.element.className;
				} else /* if (arguments.length === 1) */{
					this.element.className = newClass;
				}
			}

			/**
			 * Attaches an event handler that is called when the mouse is pressed over the element.
			 * 
			 * @param {() => void} callback the function to call
			 * 
			 * @memberOf Element
			 */
			public mousePressed(callback: () => void) {
				this.attachListener("mousedown", callback);
				this.attachListener("touchstart", callback);
			}

			public mouseWheel(callback: () => void) {
				this.attachListener("mousewheel", callback);
			}

			public mouseReleased(callback: () => void) {
				this.attachListener('mouseup', callback);
				this.attachListener('touchend', callback);
			}

			public mouseClicked(callback: () => void) {
				this.attachListener("mouseclick", callback);
			}

			public mouseMoved(callback: () => void) {
				this.attachListener("mousemove", callback);
			}

			public mouseOver(callback: () => void) {
				this.attachListener("mouseover", callback);
			}

			public changed(callback: () => void) {
				this.attachListener("change", callback);
			}

			public input(callback: () => void) {
				this.attachListener("input", callback);
			}

			public mouseOut(callback: () => void) {
				this.attachListener("mouseout", callback);
			}

			public readonly touchStarted = this.mousePressed;
			public readonly touchMoved = this.mouseMoved;
			public readonly touchEnded = this.mouseReleased;

			public dragOver(callback: () => void) {
				this.attachListener("dragover", callback);
			}

			public dragLeave(callback: () => void) {
				this.attachListener("dragleave", callback);
			}

			private attachListener(eventName: string, callback: () => void) {
				this.element.addEventListener(eventName, () => callback(), false);
				this.events[eventName] = callback;
			}
		}
	}
}
